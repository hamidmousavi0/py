import torch
import torchvision
import torch.optim as optim
import torchvision.transforms as transforms
import torch.nn as nn
import torch.nn.functional as F
import numpy
from Data_load import load_Cifar10

def compute_accuracy(model, data_loader, device):
    correct_pred, num_examples = 0, 0
    for i, (features, targets) in enumerate(data_loader):
        features = features.to(device)
        targets = targets.to(device)

        logits, probas = model(features)
        _, predicted_labels = torch.max(probas, 1)
        num_examples += targets.size(0)
        correct_pred += (predicted_labels == targets).sum()
    return correct_pred.float() / num_examples * 100


class Net(nn.Module):
    def __init__(self):
        super(Net, self).__init__()
        self.classifier = nn.Sequential(
                nn.Conv2d(3, 192, kernel_size=5, stride=1, padding=2),
                nn.ReLU(inplace=True),
                nn.Conv2d(192, 160, kernel_size=1, stride=1, padding=0),
                nn.ReLU(inplace=True),
                nn.Conv2d(160,  96, kernel_size=1, stride=1, padding=0),
                nn.ReLU(inplace=True),
                nn.MaxPool2d(kernel_size=3, stride=2, padding=1),
                nn.Dropout(0.5),

                nn.Conv2d(96, 192, kernel_size=5, stride=1, padding=2),
                nn.ReLU(inplace=True),
                nn.Conv2d(192, 192, kernel_size=1, stride=1, padding=0),
                nn.ReLU(inplace=True),
                nn.Conv2d(192, 192, kernel_size=1, stride=1, padding=0),
                nn.ReLU(inplace=True),
                nn.AvgPool2d(kernel_size=3, stride=2, padding=1),
                nn.Dropout(0.5),

                nn.Conv2d(192, 192, kernel_size=3, stride=1, padding=1),
                nn.ReLU(inplace=True),
                nn.Conv2d(192, 192, kernel_size=1, stride=1, padding=0),
                nn.ReLU(inplace=True),
                nn.Conv2d(192,  10, kernel_size=1, stride=1, padding=0),
                nn.ReLU(inplace=True),
                nn.AvgPool2d(kernel_size=8, stride=1, padding=0),

                )

    def forward(self, x):
        x = self.classifier(x)
        logits = x.view(x.size(0), 10)
        probas = torch.softmax(logits, dim=1)
        return logits, probas


def train_eval_NIN(model,NUM_EPOCHS,train_loader,valid_loader,training=False):
    DEVICE = "cuda" if torch.cuda.is_available() else "cpu"
    LEARNING_RATE = 0.0005
    optimizer = torch.optim.Adam(model.parameters(), lr=LEARNING_RATE)
    if training:
        for epoch in range(NUM_EPOCHS):

            model.train()

            for batch_idx, (features, targets) in enumerate(train_loader):

                ### PREPARE MINIBATCH
                features = features.to(DEVICE)
                targets = targets.to(DEVICE)

            ### FORWARD AND BACK PROP
                logits, probas = model(features)
                cost = F.cross_entropy(logits, targets)
                optimizer.zero_grad()

                cost.backward()

                ### UPDATE MODEL PARAMETERS
                optimizer.step()

            ### LOGGING
                if not batch_idx % 120:
                    print(f'Epoch: {epoch + 1:03d}/{NUM_EPOCHS:03d} | '
                          f'Batch {batch_idx:03d}/{len(train_loader):03d} |'
                          f' Cost: {cost:.4f}')
        torch.save(model,"/.NIN.pth")
    else:
        torch.load("/.NIN.pth")
    return model,compute_accuracy(model,valid_loader,DEVICE)
if __name__ == '__main__':
    train_loader,valid_loader = load_Cifar10()
    model = Net()
    model, acc = train(model,100,train_loader,valid_loader)
    print(acc)

