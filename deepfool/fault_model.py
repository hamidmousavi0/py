from pytorchfi.core import fault_injection as pfi_core
from LENET import train_eval_lenet
from Data_load import load_mnist
import torch
def fault_model_one_weight(model,img_size=32,BATCH_SIZE=1,channel=3,conv_i = 1,k = 15,c_i = 20,h_i = 2,w_i = 3,value=10000):
    p = pfi_core(model,img_size,img_size,BATCH_SIZE,USE_GPU=False,c=channel)
    inj_model = p.declare_weight_fi(
            conv_num=conv_i, k=k, c=c_i, h=h_i, w=w_i,value=value )
    return  inj_model
def fault_model_one_neroun(model,img_size=32,BATCH_SIZE=1,channel=3,b=0,conv_i = 1,c_i = 20,h_i = 2,w_i = 3,value=1000):
    p = pfi_core(model, img_size, img_size, BATCH_SIZE, USE_GPU=False, c=channel)
    inj_model = p.declare_neuron_fi(batch=b,conv_num=conv_i,c=c_i,h=h_i,w=w_i,value=value)
    return  inj_model


if __name__ == '__main__':
    model,acc = train_eval_lenet()
    print(model.feature_extractor[0])
    for i in model.parameters():
        print(i.size())
    train,test = load_mnist()
    model_inj = fault_model_one_weight(model,img_size=32,BATCH_SIZE=32,channel=1,conv_i=[0,1,2,1,1,1,1,1,1,1],k=[1,1,1,1,1,1,1,1,1,1],c_i=[1,1,1,1,1,1,1,1,1,1],h_i=[1,1,1,1,1,1,1,1,1,1],w_i=[1,1,1,1,1,1,1,1,1,1],value=[10000,10000,-1000,1000,-1000,1000,10000,-10000,10000,1000])
    iter_test = iter(test)
    images , labels = next(iter_test)
    model_inj.eval()
    _, y_prob = model_inj(images)
    _, predicted_labels = torch.max(y_prob, 1)
    print(predicted_labels)
    print(labels)
    # model.eval()
    # _, y_prob = model(images)
    # _, predicted_labels = torch.max(y_prob, 1)
    # print(predicted_labels)





